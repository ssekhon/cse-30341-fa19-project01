/* idlebin.c: idlebin */

#include "idlebin.h"

#include <libgen.h>

/* Usage */

void idlebin_usage(int status) {
    fprintf(stderr, "Usage: idlebin applet [ARGUMENTS]...\n");
    fprintf(stderr, "Applets:\n");
    for (size_t i = 0; APPLETS[i].name; i++) {
        fprintf(stderr, "    %s\n", APPLETS[i].name);
    }
    exit(status);
}

/* Main Execution */

int main(int argc, char **argv) {
    char *applet_name = basename(argv[0]);

    /* Check if argv[0] is "idlebin" or "applet" */
    if (streq(applet_name, "idlebin")) {
    	if (argc > 1) {
    	    applet_name = argv[1];
    	    argc--;
    	    argv++;
	} else {
	    idlebin_usage(EXIT_FAILURE);
	}
    }

    /* Search for applet */
    AppletFunction applet_function = NULL;
    const char *   applet_usage    = NULL;
    for (size_t i = 0; APPLETS[i].name; i++) {
        if (streq(applet_name, APPLETS[i].name)) {
            applet_function = APPLETS[i].function;
            applet_usage    = APPLETS[i].usage;
            break;
        }
    }

    if (!applet_function) {
        idlebin_usage(EXIT_FAILURE);
    }

    /* Check for --help message */
    for (size_t i = 1; i < argc; i++) {
        if (streq(argv[i], "--help")) {
            display_usage(applet_usage, EXIT_SUCCESS);
        }
    }

    /* Dispatch applet */
    return applet_function(argc, argv);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
